class Solution:
    def __init__(self):
        self.output = ""
        self.breakFlag = False
        self.markFlag = False

    def myAtoi(self, str: str) -> int:
        for i in str:
            if i == " " and self.breakFlag is False:
                continue
            elif i in ['+', '-'] and self.markFlag is False:
                self.output += i
                self.breakFlag = True
                self.markFlag = True
            elif self.isInt(i):
                self.output += i
                self.breakFlag = True
                self.markFlag = True
            else:
                return self.return_output()

        return self.return_output()

    def return_output(self):
        return self.valid_output() if self.isInt(self.output) else 0

    def valid_output(self):
        output = int(self.output)

        if output > 0:
            return output if output < 2**31 - 1 else 2**31 - 1
        else:
            return output if output > -2**31 else -2**31

    def isInt(self, value):
        try:
            int(value)
            return True
        except ValueError:
            return False


# Fast realization
class Solution1:
    def myAtoi(self, str: str) -> int:
        digits = {'0':0, '1':1, '2':2, '3':3, '4':4, '5':5, '6':6, '7':7, '8':8, '9':9}
        MIN_INT, MAX_INT = -2**31, 2**31-1
        i = 0
        while i<len(str) and str[i] == ' ':
            i += 1
        sign = 1
        if i<len(str) and str[i] in '+-':
            if str[i]=='-': sign = -1
            i += 1
        res = 0
        while i<len(str) and str[i] in digits:
            res = 10*res + digits[str[i]]
            i += 1
        return max(min(sign*res, MAX_INT), MIN_INT)

if __name__ == "__main__":
    sol = Solution()

    str1 = "      42"
    str2 = "4193 with words"
    str3 = "words and 987"
    str4 = "-987"
    str5 = "-91283472332"
    str6 = "-"
    str7 = "+1"
    str8 = "+-2"
    str9 = "   +0 123"
    str10 = "-5-"

    # print(sol.myAtoi(str1))
    # print(sol.myAtoi(str2))
    # print(sol.myAtoi(str3))
    # print(sol.myAtoi(str4))
    print(sol.myAtoi(str10))