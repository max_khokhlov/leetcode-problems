class Solution:
    def reverse(self, x: int) -> int:
        x = str(x)
        x = x[::-1]
        res = 0

        if x[-1] == "-":
            res = int(x[:-1]) * -1
        else:
            res = int(x)

        if res > (2 ** 31) or res < (-2 ** 31):
            return 0
        else:
            return res

if __name__ == '__main__':
    sol = Solution()
    print(sol.reverse(1534236469))
    print(sol.reverse(-321))
    print(sol.reverse(-120))
    print(sol.reverse(1000))