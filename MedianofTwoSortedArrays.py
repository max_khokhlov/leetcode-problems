from typing import List


class Solution:

    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:
        median = self.find_median([*nums1, *nums2])
        return median

    def find_median(self, nums: List[int]) -> float:

        if len(nums) == 0:
            return 0

        nums = sorted(nums)
        n = len(nums)
        midpoint = n // 2

        if n % 2 == 1:
            return nums[midpoint]
        else:
            lo = midpoint - 1
            hi = midpoint
            return (nums[lo] + nums[hi]) / 2


if __name__ == '__main__':
    sol = Solution()

    list1 = [3]
    list2 = [-2,-1]

    res = sol.findMedianSortedArrays(list1, list2)
    print(res)
