class Solution:

    def __init__(self):
        self.zigzag = 0
        self.down = False

    def convert(self, s: str, numRows: int) -> str:

        if numRows == 1:
            return s

        obj = {index: "" for index in range(numRows)}

        for index in range(len(s)):
            obj[self.zigzag] += s[index]
            self.get_key(numRows)

        return "".join(obj.values())

    def get_key(self, numRows: int) -> int:
        if numRows - 1 == self.zigzag or self.zigzag == 0:
            self.down = not self.down

        if self.down:
            self.zigzag += 1
        else:
            self.zigzag -= 1

        return self.zigzag



if __name__ == '__main__':
    sol = Solution()

    testString = "PAYPALISHIRING"

    resStr = "PAHNAPLSIIGYIR"

    print(sol.convert(testString, 3), resStr)
    print(sol.convert(testString, 4))

"""
    P   A   H   N
    A P L S I I G
    Y   I   R
"""

"""
    P     I    N
    A   L S  I G
    Y A   H R
    P     I
"""