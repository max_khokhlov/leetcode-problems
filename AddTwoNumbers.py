

# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:

    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        number1 = self.list_print_to_string(l1)
        number2 = self.list_print_to_string(l2)
        summ = int(number1) + int(number2)
        return self.create_linking_list(str(summ))

    def list_print_to_string(self, list: ListNode):
        printlist = list
        return_string = ""
        while printlist is not None:
            return_string += str(printlist.val)
            printlist = printlist.next
        return return_string[::-1]

    def create_linking_list(self, string):
        last = ListNode(string[0])
        list_return = None
        for index in range(1, len(string)):
            list_return = ListNode(string[index])
            list_return.next = last
            last = list_return
        return list_return if list_return is not None else last

#
# [2,4,3]
# [5,6,4]
#
if __name__ == '__main__':

    ln2_1 = ListNode(2)
    ln4_1 = ListNode(4)
    ln4_1.next = ListNode(3)

    ln2_1.next = ln4_1

    ln5_2 = ListNode(5)
    ln6_2 = ListNode(6)
    ln6_2.next = ListNode(4)

    ln5_2.next = ln6_2

    solution = Solution()
    result = solution.addTwoNumbers(ListNode(0), ListNode(0))
    print(solution.list_print_to_string(result))