import random


def quicksort(arr):

    if len(arr) < 1:
        return arr

    median = len(arr) // 2
    left_arr = []
    right_arr = []
    med_arr = []

    for i in arr:
        if i < arr[median]:
            left_arr.append(i)
        elif i > arr[median]:
            right_arr.append(i)
        else:
            med_arr.append(i)

    return quicksort(left_arr) + med_arr + quicksort(right_arr)


if __name__ == "__main__":
    sort_arr = quicksort([4,5,1,2,9,8,4,6,8,0,2,55,77,33,4,55])
    print(sort_arr)
